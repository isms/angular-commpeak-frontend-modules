import DialogController from 'modules/dialog/controllers/dialog.controller';

class StoreController extends DialogController {
    constructor($scope, TicketService) {
        'ngInject';
        super($scope);
        Object.assign(this, {TicketService});
    }
    submit() {
        this.promise = this.TicketService.store(this.request).then(() => this.resolve({}));
    }   
}
export default StoreController;