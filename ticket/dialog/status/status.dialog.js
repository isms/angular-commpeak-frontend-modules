import template from './status.dialog.pug';
import controller from './status.dialog.controller';

let StatusDialogComponent = {
    template: template({}),
    controller,
    name: 'nlc-status-dialog',
};

export default StatusDialogComponent;


