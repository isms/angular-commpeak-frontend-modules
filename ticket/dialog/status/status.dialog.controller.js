import angular from 'angular';
import DialogController from 'modules/dialog/controllers/dialog.controller';

class StatusController extends DialogController {
    constructor($scope, TicketService, ticket, EventService) {
        'ngInject';
        super($scope);
        Object.assign(this, {TicketService, request: angular.copy(ticket), EventService});
    }
    submit() {
        this.promise = this.TicketService.status(this.request.id, this.request)
            .then(ticket => {
                if (angular.isDefined(this.request.comment) && this.request.comment !== '') {
                    this.EventService.fire('tickets.' + ticket.id + '.comments.updated');
                }
                this.resolve(ticket)
            });
    }
}
export default StatusController;