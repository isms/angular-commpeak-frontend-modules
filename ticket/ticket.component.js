import template from './ticket.pug';
import controller from './ticket.controller';
import './ticket.scss';

/**
 * Ticket component
 */
let TicketComponent = {
    template: template({}),
    controller,
    name: 'nlc-ticket',
}
export default TicketComponent;

