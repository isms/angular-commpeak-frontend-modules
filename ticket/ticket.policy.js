import RestPolicy from 'services/rest.policy';
class TicketPolicy extends RestPolicy {
    constructor(IdentityService) {
        'ngInject';
        super(IdentityService);
    }
    get access() {
        return ['admin', 'manager'].indexOf(this.user.role) != -1;
    }
    store() {
        return this.access;
    }
    status(ticket ={}) {
        return this.access;
    }
    update(ticket = {}) {
        return this.access;
    }
    destroy(ticket = {}) {
        return this.access;
    }
    import() {
        return this.access;
    }
    view() {
        return this.access;
    }
}
export default TicketPolicy;