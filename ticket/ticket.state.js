import TicketComponent from './ticket.component';
/**
 * TICKET state
 */
let TicketState = {
    name: 'app.ticket',
    url: '/tickets/',
    template: `<${ TicketComponent.name }/>`,
    title: 'TICKET.TITLE',
    resolve: {
        //load translations async
        translations: /*@ngInject*/(TranslationFactory) => TranslationFactory.load(require.context("./i18n", false, /^\.\/.*\.json$/)),
    },
    /**
     * Setup breadcrumbs
     */
    ncyBreadcrumb: {
        label: 'TICKET.BREADCRUMB', // angular-breadcrumb's configuration
    },
    /**
     * Setup menu
     */
    menu: {
        content: 'COMMON.MENU.TICKET',
        priority: 0,
        roles: ['admin', 'manager'],
        tag: 'sidebar topmenu',
        icon: 'fa fa-ticket',
    },
}
export default TicketState;