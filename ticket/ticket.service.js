import RestService from 'services/rest.service';

class TicketService extends RestService {
    constructor($http, API_ENDPOINT) {
        'ngInject';
        super($http, API_ENDPOINT);
    }
    endpoint(data = {}) {
        return `${this.API_ENDPOINT}tickets/`;
    }
    import() {
        return this.$http.post(this.endpoint() + 'import/').then(({data}) => data);
    }
    waiting() {
        return this.$http.post(this.endpoint() + 'waiting/').then(({data}) => data);
    }
    status(id, data) {
        return this.$http.post(this.endpoint() + id + '/status/', data).then(({data}) => data);
    }
}
export default TicketService;