import angular from 'angular';
import TicketState from './ticket.state';
import TicketComponent from './ticket.component';
import TicketService from './ticket.service';
import TicketPolicy from './ticket.policy';
import TicketComponents from './components/components';
/**
 * 
 * Ticket module
 */
let Ticket = angular.module('app.pages.ticket', [
    TicketComponents.name,
])
    .config($stateProvider => {
        'ngInject';
        $stateProvider.state(TicketState.name, TicketState)
    })
    .service('TicketService', TicketService)
    .service('TicketPolicy', TicketPolicy)
    .component(TicketComponent.name.camelCase(), TicketComponent)

export default Ticket;