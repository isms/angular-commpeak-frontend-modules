import template from './view.pug';
import controller from './view.controller';
import './view.scss';

/**
 * View component
 */
let ViewComponent = {
    template: template({}),
    controller,
    name: 'nlc-ticket-view',
    bindings: {
        ticket: '=',
    },
    bindToController: true,
}
export default ViewComponent;

