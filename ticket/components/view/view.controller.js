/**
 * View controller
 */
class ViewController {
    constructor() {
        'ngInject';
        Object.assign(this, {});
    }
    $onInit() {
    }
}
export default ViewController;