import angular from 'angular';
import ViewComponent from './view/view.component';
import CommentComponent from './comment/comment.component';
import CommentService from './comment/comment.service';
/**
 * 
 * Ticket components module
 */
let TicketComponents = angular.module('app.pages.ticket.components', [])
    .component(ViewComponent.name.camelCase(), ViewComponent)
    .service('CommentService', CommentService)
    .component(CommentComponent.name.camelCase(), CommentComponent)

export default TicketComponents;