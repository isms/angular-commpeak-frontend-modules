import RestService from 'services/rest.service';

class CommentService extends RestService {
    constructor($http, API_ENDPOINT) {
        'ngInject';
        super($http, API_ENDPOINT);
    }
    endpoint(data = {}) {
        return `${this.API_ENDPOINT}tickets/` + data.ticket_id + '/comments/';
    }
}
export default CommentService;