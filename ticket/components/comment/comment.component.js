import template from './comment.pug';
import controller from './comment.controller';
import './comment.scss';

/**
 * Comment component
 */
let CommentComponent = {
    template: template({}),
    controller,
    name: 'nlc-ticket-comments',
    bindings: {
        ticket: '=',
    },
    bindToController: true,
}
export default CommentComponent;

