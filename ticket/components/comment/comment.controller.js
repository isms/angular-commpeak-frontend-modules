import DataController from 'controllers/data.controller';
/**
 * View controller
 */
class CommentController extends DataController { 
    constructor(CommentService, EventService, TranslateDialogService) {
        'ngInject';
        super(CommentService, 1, 1000);
        Object.assign(this, {dialog: TranslateDialogService, EventService});
    }
    init() {
        Object.assign(this.request, {ticket_id: this.ticket.id, 'withs[]': 'user'});
    }
    $onInit() {
        this.init();
        this.subs = this.EventService.out.filter(event => event.type == 'tickets.'+this.ticket.id+'.comments.updated').subscribe(() => this.refresh());
        this.refresh();
    }
    $onDestroy() {
        this.subs.dispose();
    }
    store() {
        this.sp = this.service.store(this.request)
            .then(() => this.request.content = '')
            .then(() => this.refresh());
    }
}
export default CommentController;