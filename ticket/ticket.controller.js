import DataController from 'controllers/data.controller';
import StoreDialogComponent from './dialog/store/store.dialog';
import StatusDialogComponent from './dialog/status/status.dialog';
/**
 * Ticket controller
 */
class TicketController extends DataController {
    constructor(TranslateDialogService, TicketService, TicketPolicy, $state, $interval) {
        'ngInject';
        super(TicketService);
        Object.assign(this, {dialog: TranslateDialogService, policy: TicketPolicy, $interval, $state});
        this.request['withs[]'] = 'requester';
    }
    clear() {
        Object.assign(this, {imports: {count: 0, updated_at: new Date()}});
    }
    $onInit() {
        this.clear();
        this.refresh();
        //per 10 minutes
        this.$interval(() => this.waiting(), 10 * 60 * 1000);
        this.waiting();
    }
    waiting() {
        return this.service.waiting().then(imports => this.imports = imports);
    }
    store() {
        return this.dialog.open(StoreDialogComponent).then(() => this.waiting());
    }

    status(ticket) {
        return this.dialog.open(StatusDialogComponent, {ticket}).then(({status}) => ticket.status = status);
    }
    import() {
        return this.service.import().then(() => this.clear()).then(() => this.refresh());
    }

    view(ticket) {
        ticket.show = !ticket.show;
    }

    destroy(ticket) {
        return this.dialog.confirm('TICKET.GRID.ACTIONS.DESTROY.TITLE', 'TICKET.GRID.ACTIONS.DESTROY.MESSAGE', {}, ticket)
            .then(() => this.service.destroy(ticket.id, ticket))
            .then(() => this.refresh());
    }
}
export default TicketController;