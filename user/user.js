import angular from 'angular';
import UserState from './user.state';
import UserComponent from './user.component';
import UserService from './user.service';
import UserPolicy from './user.policy';
/**
 * 
 * User module
 */
let User = angular.module('app.pages.user', [])
    .config($stateProvider => {
        'ngInject';
        $stateProvider.state(UserState.name, UserState)
    })
    .service('UserService', UserService)
    .service('UserPolicy', UserPolicy)
    .component(UserComponent.name.camelCase(), UserComponent)

export default User;