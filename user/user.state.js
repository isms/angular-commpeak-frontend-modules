import UserComponent from './user.component';
/**
 * USER state
 */
let UserState = {
    name: 'app.user',
    url: '/users/',
    template: `<${ UserComponent.name }/>`,
    title: 'USER.TITLE',
    resolve: {
        //load translations async
        translations: /*@ngInject*/(TranslationFactory) => TranslationFactory.load(require.context("./i18n", false, /^\.\/.*\.json$/)),
    },
    /**
     * Setup breadcrumbs
     */
    ncyBreadcrumb: {
        label: 'USER.BREADCRUMB', // angular-breadcrumb's configuration
    },
    /**
     * Setup menu
     */
    menu: {
        content: 'COMMON.MENU.USER',
        priority: 1,
        roles: ['admin'],
        tag: 'sidebar',
        icon: 'fa fa-users',
    },
}
export default UserState;