import RestPolicy from 'services/rest.policy';
class UserPolicy extends RestPolicy {
    constructor(IdentityService) {
        'ngInject';
        super(IdentityService);
    }
    update(user) {
        return this.user.role === 'admin';
    }
    destroy(user) {
        return this.user.id != user.id;
    }
}
export default UserPolicy;