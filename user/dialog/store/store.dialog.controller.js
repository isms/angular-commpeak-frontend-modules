import DialogController from 'modules/dialog/controllers/dialog.controller';

class StoreController extends DialogController {
    constructor($scope, UserService) {
        'ngInject';
        super($scope);
        Object.assign(this, {UserService});
    }
    submit() {
        this.promise = this.UserService.store(this.request).then(user => this.resolve(user));
    }
}
export default StoreController;