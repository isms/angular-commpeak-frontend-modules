import template from './store.dialog.pug';
import controller from './store.dialog.controller';

let StoreDialogComponent = {
    template: template({}),
    controller,
    name: 'nlc-store-dialog',
};

export default StoreDialogComponent;


