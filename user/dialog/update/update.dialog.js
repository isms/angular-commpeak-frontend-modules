import template from './update.dialog.pug';
import controller from './update.dialog.controller';

let UpdateDialogComponent = {
    template: template({}),
    controller,
    name: 'nlc-update-dialog',
};

export default UpdateDialogComponent;


