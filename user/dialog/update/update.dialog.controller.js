import angular from 'angular';
import DialogController from 'modules/dialog/controllers/dialog.controller';

class UpdateController extends DialogController {
    constructor($scope, UserService, user) {
        'ngInject';
        super($scope);
        Object.assign(this, {UserService, request: angular.copy(user)});
    }
    submit() {
        this.promise = this.UserService.update(this.request.id, this.request).then(user => this.resolve(user));
    }
    
}
export default UpdateController;