import RestService from 'services/rest.service';

class UserService extends RestService {
    constructor($http, API_ENDPOINT) {
        'ngInject';
        super($http, API_ENDPOINT);
    }
    endpoint(data = {}) {
        return `${this.API_ENDPOINT}users/`;
    }
}
export default UserService;