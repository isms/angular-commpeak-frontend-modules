import template from './user.pug';
import controller from './user.controller';
import './user.scss';

/**
 * User component
 */
let UserComponent = {
    template: template({}),
    controller,
    name: 'nlc-user',
}
export default UserComponent;

