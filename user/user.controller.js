import DataController from 'controllers/data.controller';
import StoreDialogComponent from './dialog/store/store.dialog';
import UpdateDialogComponent from './dialog/update/update.dialog';
/**
 * User controller
 */
class UserController extends DataController {
    constructor(TranslateDialogService, UserService, UserPolicy) {
        'ngInject';
        super(UserService);
        Object.assign(this, {dialog: TranslateDialogService, policy: UserPolicy});
    }
    store() {
        return this.dialog.open(StoreDialogComponent).then(() => this.refresh());
    }
    update(user) {
        return this.dialog.open(UpdateDialogComponent, {user}).then(() => this.refresh());
    }
    destroy(user) {
        return this.dialog
            .confirm('USER.GRID.ACTIONS.DESTROY.TITLE', 'USER.GRID.ACTIONS.DESTROY.MESSAGE', {}, user/*interpolation params*/)
            .then(() => this.service.destroy(user.id, user))
            .then(() => this.refresh());
    }
}
export default UserController;