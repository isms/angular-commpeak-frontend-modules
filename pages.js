import angular from 'angular';
import User from './user/user';
import Ticket from './ticket/ticket';

let Pages = angular.module('app.pages', [
    User.name,
    Ticket.name,
])
export default Pages;